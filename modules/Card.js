import {Text, View, StyleSheet } from 'react-native';

export  function Card({name, antoColor}) {
  return (
    <View style={[styles.container, {backgroundColor: antoColor }]}>
        <Text>{name}</Text>
    </View>
  );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#EEE',
      alignItems: 'center',
      justifyContent: 'center',
      margin:10,
    },
  });
  