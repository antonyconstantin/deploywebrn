import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Card } from './modules/Card';
export default function App() {
  return (
    <View style={styles.container}>
      <Card name="toto" antoColor="yellow" />
      <Card name="tata" antoColor="red" />
      <Card name="papa" antoColor="blue" />
      <Card  name="mama" antoColor="red" />
      <Card  name="test"/>
      <StatusBar style="auto" antoColor="#333" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '',
  },
});
